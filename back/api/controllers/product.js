module.exports = (db) => {
  // To Search by Keywords
  const searchByKeyword = async (req, res) => {
    // Setting Variables
    const { Op } = db.sequelize;
    const keywordQuery = req.query.search;
    let offsetQuery = req.query.page;

    if (typeof (offsetQuery) === 'undefined' || offsetQuery === '') offsetQuery = 0;
    if (typeof (keywordQuery) === 'undefined' || keywordQuery === '') return res.status(400).json({ status: 'bad request' });

    // Executing Query
    try {
      const products = await db.Product.findAll({
        attributes: ['id', 'name', 'price', 'image_url', 'in_stock'],
        where: {
          [Op.or]: [
            { name: { [Op.like]: `%${keywordQuery}%` } },
            { brand: { [Op.like]: `%${keywordQuery}%` } },
          ],
        },
        limit: 12,
        offset: (offsetQuery === '1') ? 0 : offsetQuery * 12,
      });

      const countProducts = await db.Product.findAll({
        attributes: [['count(*)', 'count']],
        where: {
          [Op.or]: [
            { name: { [Op.like]: `%${keywordQuery}%` } },
            { brand: { [Op.like]: `%${keywordQuery}%` } },
          ],
        },
      });

      // For pagination
      const actualPage = (offsetQuery === 0) ? 1 : offsetQuery;
      const count = Number(countProducts[0].dataValues.count);
      const pages = (count > 12) ? Math.floor(count / 12) : 1;

      return res.status(200).send({
        products,
        pagination: { page: actualPage, total: pages, count },
      });
    } catch (err) {
      return res.status(500).send(err);
    }
  };

  // Products filtering by Brand
  const listRandomProducts = (req, res) => db.Product.findAll({
    attributes: ['id', 'name', 'price', 'image_url', 'in_stock'],
    limit: 12,
    order: db.sequelize.fn('RANDOM'),

  })
    .then(products => res.status(200).send(products))
    .catch(err => res.status(500).send(err));

  // Products filtering by Brand
  const listProductsByBrand = async (req, res) => {
    // Setting Variables
    const brandQuery = req.params.brand;
    let offsetQuery = req.query.page;

    if (typeof (offsetQuery) === 'undefined' || offsetQuery === '') offsetQuery = 0;
    if (typeof (brandQuery) === 'undefined' || brandQuery === '') return res.status(400).json({ status: 'bad request' });

    // Executing Query
    try {
      const products = await db.Product.findAll({
        attributes: ['id', 'name', 'price', 'image_url', 'in_stock'],
        where: { brand: brandQuery },
        limit: 12,
        offset: (offsetQuery === '1') ? 0 : offsetQuery * 12,
      });

      const countProducts = await db.Product.findAll({
        attributes: [['count(*)', 'count']],
        where: { brand: brandQuery },
      });

      // For pagination
      const actualPage = (offsetQuery === 0) ? 1 : offsetQuery;
      const count = Number(countProducts[0].dataValues.count);
      const pages = (count > 12) ? Math.floor(count / 12) : 1;

      return res.status(200).send({
        products,
        pagination: { page: actualPage, total: pages, count },
      });
    } catch (err) {
      return res.status(500).send(err);
    }
  };

  // Products filtering by Category
  const listProductsByCategory = async (req, res) => {
    // Setting Variables
    const { Op } = db.sequelize;
    const categoryQuery = req.params.category;
    let offsetQuery = req.query.page;


    if (typeof (offsetQuery) === 'undefined' || offsetQuery === '') offsetQuery = 0;
    if (typeof (categoryQuery) === 'undefined' || categoryQuery === '') return res.status(400).json({ status: 'bad request' });

    // Executing Query
    try {
      const products = await db.Product.findAll({
        attributes: ['id', 'name', 'price', 'image_url', 'in_stock'],
        where: { categories: { [Op.like]: `%${categoryQuery}%` } },
        limit: 12,
        offset: (offsetQuery === '1') ? 0 : offsetQuery * 12,
      });

      const countProducts = await db.Product.findAll({
        attributes: [['count(*)', 'count']],
        where: { categories: { [Op.like]: `%${categoryQuery}%` } },
      });

      // For pagination
      const actualPage = (offsetQuery === 0) ? 1 : offsetQuery;
      const count = Number(countProducts[0].dataValues.count);
      const pages = (count > 12) ? Math.floor(count / 12) : 1;

      return res.status(200).send({
        products,
        pagination: { page: actualPage, total: pages, count },
      });
    } catch (err) {
      return res.status(500).send(err);
    }
  };

  // List Brands
  const listBrands = (req, res) => {
    // Setting Variables
    const { Op } = db.sequelize;

    return db.Product.findAll({
      attributes: ['brand'],
      where: {
        brand: {
          [Op.ne]: 'null',
        },
      },
      group: ['brand'],
      order: ['brand'],
    })
      .then(brands => res.status(200).send(brands))
      .catch(err => res.status(500).send(err));
  };
  // List Categories
  const listCategories = (req, res) => db.Product.findAll({
    attributes: ['categories'],
    group: ['categories'],
    order: ['categories'],
  })
    .then((categories) => {
      const categoriesList = [];

      categories.forEach((categoryGrouped) => {
        // Split Category String in an Array
        categoryGrouped.categories.split('|').forEach((categorySolo) => {
          categoriesList.push({ category: categorySolo });
        });
      });

      // Sort by name
      categoriesList.sort((cat1, cat2) => ((cat1.category > cat2.category) ? 1 : -1));

      // Remove Duplicates
      /* eslint-disable max-len */
      const categoriesUnique = categoriesList.filter((item, index) => index === categoriesList.findIndex(obj => obj.category === item.category));
      /* eslint-enable max-len */
      res.status(200).send(categoriesUnique);
    })
    .catch(err => res.status(500).send(err));

  return {
    searchByKeyword,
    listProductsByBrand,
    listProductsByCategory,
    listBrands,
    listCategories,
    listRandomProducts,
  };
};
