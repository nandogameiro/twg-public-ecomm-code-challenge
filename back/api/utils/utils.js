// Handle 404 error
const handleNotFound = (req, res) => {
  res.status(404).json({
    status: 'error',
    code: 404,
    message: 'Endpoint not found.',
  });
};

// Handle 500 error
const handleServerBroken = (error, req, res) => {
  res.status(500).send({
    status: 'error',
    code: 500,
    message: error,
  });
};

module.exports = {
  handleNotFound,
  handleServerBroken,
};
