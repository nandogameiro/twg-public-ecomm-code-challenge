
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    date_added: DataTypes.DATE,
    type: DataTypes.STRING,
    sku: DataTypes.STRING,
    group_id: DataTypes.STRING,
    name: DataTypes.STRING,
    model: DataTypes.STRING,
    brand: DataTypes.STRING,
    url: DataTypes.STRING,
    image_url: DataTypes.STRING,
    overlay_image_url: DataTypes.STRING,
    overlay_image_width: DataTypes.INTEGER,
    overlay_image_height: DataTypes.INTEGER,
    on_special: DataTypes.BOOLEAN,
    on_special_offer: DataTypes.STRING,
    on_special_text: DataTypes.STRING,
    on_special_ends: DataTypes.STRING,
    price: DataTypes.FLOAT,
    flybuys_points: DataTypes.INTEGER,
    shipping_class: DataTypes.STRING,
    add_to_cart: DataTypes.BOOLEAN,
    categories: DataTypes.STRING,
    in_stock: DataTypes.BOOLEAN,
    sold_out: DataTypes.BOOLEAN,
  }, { timestamps: false });
  return Product;
};
