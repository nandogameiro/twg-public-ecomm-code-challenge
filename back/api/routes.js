
const app = require('express');
const Utils = require('./utils/utils');
const db = require('./models');
const productController = require('./controllers/product')(db);


const router = app.Router();

// All Routes
router.get('/api/products/category/:category', productController.listProductsByCategory);
router.get('/api/products/brand/:brand', productController.listProductsByBrand);
router.get('/api/products/search', productController.searchByKeyword);
router.get('/api/products', productController.listRandomProducts);
router.get('/api/brands', productController.listBrands);
router.get('/api/categories', productController.listCategories);


// Default Routes
router.use('*', Utils.handleNotFound);
router.use(Utils.handleServerBroken);


module.exports = router;
