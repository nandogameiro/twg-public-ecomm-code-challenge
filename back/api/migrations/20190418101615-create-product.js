/*eslint-disable */
'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date_added: {
        type: Sequelize.DATE
      },
      type: {
        type: Sequelize.STRING
      },
      sku: {
        type: Sequelize.STRING
      },
      group_id: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      model: {
        type: Sequelize.STRING
      },
      brand: {
        type: Sequelize.STRING
      },
      url: {
        type: Sequelize.STRING
      },
      image_url: {
        type: Sequelize.STRING
      },
      overlay_image_url: {
        type: Sequelize.STRING
      },
      overlay_image_width: {
        type: Sequelize.INTEGER
      },
      overlay_image_height: {
        type: Sequelize.INTEGER
      },
      on_special: {
        type: Sequelize.BOOLEAN
      },
      on_special_offer: {
        type: Sequelize.STRING
      },
      on_special_text: {
        type: Sequelize.STRING
      },
      on_special_ends: {
        type: Sequelize.STRING
      },                  
      price: {
        type: Sequelize.FLOAT
      },
      flybuys_points: {
        type: Sequelize.INTEGER
      },
      shipping_class: {
        type: Sequelize.STRING
      },
      add_to_cart: {
        type: Sequelize.BOOLEAN
      },
      categories: {
        type: Sequelize.STRING
      },
      in_stock: {
        type: Sequelize.BOOLEAN
      },
      sold_out: {
        type: Sequelize.BOOLEAN
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Products');
  }
};