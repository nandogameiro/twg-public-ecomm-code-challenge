// Import the dependencies for testing
const chai =  require('chai');
const chaiHttp = require('chai-http');
const app =  require('../index');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("API", () => {
    describe("GET /", () => {
        // Test to get all brands
        it("should get all brands", (done) => {
             chai.request(app)
                 .get('/api/brands')
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('array');
                     done();
                  });
         });
         // Test to get all categories
         it("should get all categories", (done) => {
            chai.request(app)
                .get('/api/categories')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                 });
        });         
        // Test to get all products by specific category
        it("should get all products by specific category", (done) => {
             const category = "Action Cameras";
             chai.request(app)
                 .get(`/api/products/category/${category}`)
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     done();
                  });
         });
        // Test to get all products by specific brand
        it("should get all products by specific brand", (done) => {
            const brand = "Apple";
            chai.request(app)
                .get(`/api/products/brand/${brand}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                 });
        });
        
        // Test to get all products by specific keyword
        it("should get all products by specific keyword", (done) => {
            const keyword = "Laptop";
            chai.request(app)
                .get(`/api/products/search?search=${keyword}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                 });
        }); 
        
        // Test to get random products
        it("should get random products ", (done) => {
            chai.request(app)
                .get(`/api/products`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                 });
        });           

    });
});