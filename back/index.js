const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./api/routes');

const port = process.env.PORT || 3070;

// Instantiate express
const app = express();


app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

// Register our routes
app.use('/',routes);

// Start our server
app.listen(port, () => {
  console.log(`The Server is Running on Port: ${port}`);
});

module.exports = app;
