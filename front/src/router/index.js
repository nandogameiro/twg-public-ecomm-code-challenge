import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Search from '@/components/Search';
import ShoppingCart from '@/components/ShoppingCart';
import Categories from '@/components/Categories';
import Brands from '@/components/Brands';
import CategoryAndBrandSingle from '@/components/CategoryAndBrandSingle';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/search/:search',
      name: 'Search',
      component: Search,
    },
    {
      path: '/cart',
      name: 'ShoppingCart',
      component: ShoppingCart,
    },
    {
      path: '/categories',
      name: 'Categories',
      component: Categories,
    },
    {
      path: '/brands',
      name: 'Brands',
      component: Brands,
    },
    {
      path: '/categories/:category',
      name: 'CategorySingle',
      component: CategoryAndBrandSingle,
    },
    {
      path: '/brands/:brand',
      name: 'BrandSingle',
      component: CategoryAndBrandSingle,
    },

  ],
});
