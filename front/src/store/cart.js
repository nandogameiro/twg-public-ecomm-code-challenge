
const state = {
  items: [],
  checkoutStatus: null,
};

// getters
const getters = {
  cartProducts: (state, getters, rootState) => state.items.map(({ id, name, price, image_url, quantity }) => ({
    id,
    name,
    price,
    image_url,
    quantity,
  })),

  cartTotalPrice: (state, getters) => {
    const total = getters.cartProducts.reduce((total, product) => total + product.price * product.quantity, 0);

    return parseFloat(total).toFixed(2);
  },
};

// actions
const actions = {
  checkout({ commit, state }, products) {
    // empty cart
    commit('setCartItems', { items: [] });
  },

  addProductToCart({ state, commit }, product) {
    if (product.in_stock === true) {
      const cartItem = state.items.find(item => item.id === product.id);
      if (!cartItem) {
        commit('pushProductToCart', { id: product.id, name: product.name, price: product.price, image_url: product.image_url });
      } else {
        commit('incrementItemQuantity', cartItem);
      }
      // remove 1 item from stock
      // commit('products/decrementProductInventory', { id: product.id }, { root: true })
    }
  },

  deleteProductFromCart({ state, commit }, product) {
    commit('deleteItemCart', { id: product.id });
  },

};

// mutations
const mutations = {
  pushProductToCart(state, { id, name, price, image_url }) {
    state.items.push({
      id,
      name,
      price,
      image_url,
      quantity: 1,
    });
  },

  incrementItemQuantity(state, { id }) {
    const cartItem = state.items.find(item => item.id === id);
    cartItem.quantity++;
  },


  deleteItemCart(state, { id }) {
    state.items = state.items.filter(item => item.id != id);
  },

  setCartItems(state, { items }) {
    state.items = items;
  },

};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
