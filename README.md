## The Warehouse Group - Code Test

Code test made by Fernando to aim a position as Developer at TWG


#### Technology used in this project
 - Backend: Node.js / Express / Mocha / Chai / ESLint Airbnb
 - Frontend: Vue.js / Bulma / Axios / ESLint Airbnb



## Backend

To run unit tests execute commands below:

```bash
cd back
npm run test
```

To start the backend execute commands below:

```bash
cd back
node index.js
```

The backend api will be available on http://localhost:3070 if ran locally.


## Frontend

To build and run execute commands below:

```bash
cd front
npm run build
node server.js
```

Make sure you set an env variable for the right API address in front/back/config/prod.env.js or pass directly on node command ROOT_API at build time

```bash
ROOT_API=http://address-to-backend node run build
```


## Public Deployment
Frontend: http://twg-frontend.herokuapp.com

Backend:  http://twg-backend.herokuapp.com
